# Change logs


## [0.2.0] - 18-03-2023

### CHANGED
- Tab color from ugly to better
- Terminal color readjustment
- Git decorator is now better